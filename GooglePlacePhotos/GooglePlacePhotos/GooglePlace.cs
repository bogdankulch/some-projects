﻿using GooglePlacePhotos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using static GooglePlacePhotos.PlaceClasses;

public class GooglePlace : IGooglePlaceService
{

    public string CreateReqest(string name, int radius, string language, string key)
    {
        string startRequest = @"https://maps.googleapis.com/maps/api/place/textsearch/json?";

        var place = "input=" + string.Join("|", name);
        var radiusPlace = "&radius=" + string.Join("|", radius);

        string endRequest = @"&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry";

        return startRequest + place + radiusPlace + endRequest + "&language=" + language + "&key=" + key;

    }

    public Result Responce(string json)
    {
        return JsonConvert.DeserializeObject<Result>(json);
    }

    public string Request(string request)
    {
        HttpWebRequest url = (HttpWebRequest)WebRequest.Create(request);

        string responseContent = null;


        using (WebResponse response = url.GetResponse())
        {
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader streamreader = new StreamReader(stream))
                {
                    responseContent = streamreader.ReadToEnd();
                }
            }
        }

        return (responseContent);
    }
}

