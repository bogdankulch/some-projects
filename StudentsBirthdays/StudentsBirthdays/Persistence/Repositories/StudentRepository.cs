﻿using StudentsBirthdays.Core.Domain;
using StudentsBirthdays.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsBirthdays.Persistence.Repositories
{
    public class StudentRepository : Repository<Student>, IStudentRepository
    {
        public StudentsBirthdaysContext db => Context as StudentsBirthdaysContext;

        public StudentRepository(StudentsBirthdaysContext context) : base(context)
        {
        }

        public List<string> GetStudentsBirthdayInCurrentMonth()
        {
            List<string> studBirthCurr = new List<string>();
            studBirthCurr = db.Students.Where(b => b.Birthday.Month == DateTime.Now.Month).Select(b => b.FirstName + b.LastName + b.ClassID + b.Birthday).ToList();
            foreach (var b in studBirthCurr) ;
            return studBirthCurr;
        }

        public List<string> GetStudentsBirthdayInMonth()
        {
            List<string> studBirthMonth = new List<string>();
            studBirthMonth = db.Students.Where(b => SqlFunctions.DatePart("month", b.Birthday).Value == 05).Select(b => b.FirstName + b.LastName + b.ClassID + b.Birthday).ToList(); //05 = Month number
            foreach (var b in studBirthMonth) ;
            return studBirthMonth;
        }

        public List<string> GetThreeOldestStudents()
        {
            List<string> studOld = new List<string>();
            studOld = db.Students.OrderBy(b => b.Birthday).Select(b => b.FirstName + " " + b.LastName).Take(3).ToList();
            foreach (var b in studOld) ;
            return studOld;
        }

        public List<string> GetThreeYoungestStudents()
        {
            List<string> studYoun = new List<string>();
            studYoun = db.Students.OrderByDescending(b => b.Birthday).Select(b => b.FirstName + " " + b.LastName).Take(3).ToList();
            foreach (var b in studYoun) ;
            return studYoun;
        }
    }
}
