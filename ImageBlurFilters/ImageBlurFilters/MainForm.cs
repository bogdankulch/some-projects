﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

using Emgu.CV.CvEnum;
using Emgu.CV;
using Emgu.CV.Structure;

namespace ImageBlurFilter
{
    public partial class MainForm : Form
    {
        private Bitmap originalBitmap = null;
        private Bitmap previewBitmap = null;
        private Bitmap resultBitmap = null;
        
        public MainForm()
        {
            InitializeComponent();
            cmbBlurFilter.Items.Add(ExtBitmap.BlurType.Mean5x5);
            cmbBlurFilter.Items.Add(ExtBitmap.BlurType.Median5x5);
            cmbBlurFilter.SelectedIndex = 0;
        }

        private void btnOpenOriginal_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select an image file.";
            ofd.Filter = "Png Images(*.png)|*.png|Jpeg Images(*.jpg)|*.jpg";
            ofd.Filter += "|Bitmap Images(*.bmp)|*.bmp";

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamReader streamReader = new StreamReader(ofd.FileName);
                originalBitmap = (Bitmap)Bitmap.FromStream(streamReader.BaseStream);
                streamReader.Close();

                previewBitmap = originalBitmap.CopyToSquareCanvas(picPreview.Width);
                picPreview.Image = previewBitmap;

                ApplyFilter(true);
            }
        }

        private void btnSaveNewImage_Click(object sender, EventArgs e)
        {
            ApplyFilter(false);

            if (resultBitmap != null)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Title = "Specify a file name and file path";
                sfd.Filter = "Png Images(*.png)|*.png|Jpeg Images(*.jpg)|*.jpg";
                sfd.Filter += "|Bitmap Images(*.bmp)|*.bmp";

                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string fileExtension = Path.GetExtension(sfd.FileName).ToUpper();
                    ImageFormat imgFormat = ImageFormat.Png;

                    if (fileExtension == "BMP")
                    {
                        imgFormat = ImageFormat.Bmp;
                    }
                    else if (fileExtension == "JPG")
                    {
                        imgFormat = ImageFormat.Jpeg;
                    }

                    StreamWriter streamWriter = new StreamWriter(sfd.FileName, false);
                    resultBitmap.Save(streamWriter.BaseStream, imgFormat);
                    streamWriter.Flush();
                    streamWriter.Close();

                    resultBitmap = null;
                }
            }
        }

        private void ApplyFilter(bool preview)
        {
            if (previewBitmap == null || cmbBlurFilter.SelectedIndex == -1)
            {
                return;
            }

            Bitmap selectedSource = null;
            Bitmap bitmapResult = null;

            if (preview == true)
            {
                selectedSource = previewBitmap;
            }
            else
            {
                selectedSource = originalBitmap;
            }

            if (selectedSource != null)
            {
                ExtBitmap.BlurType blurType =
                    ((ExtBitmap.BlurType)cmbBlurFilter.SelectedItem);

                bitmapResult = selectedSource.ImageBlurFilter(blurType);
            }

            if (bitmapResult != null)
            {
                if (preview == true)
                {
                    picPreview.Image = bitmapResult;
                }
                else
                {
                    resultBitmap = bitmapResult;
                }
            }
        }

        private void FilterValueChangedEventHandler(object sender, EventArgs e)
        {
            ApplyFilter(true);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap baseImage;
            Bitmap overlayImage;

            baseImage = (Bitmap)Image.FromFile("image1.png");
            overlayImage = (Bitmap)Image.FromFile("image2.png");

            var finalImage = new Bitmap(overlayImage.Width, overlayImage.Height, PixelFormat.Format32bppArgb);
            var graphics = Graphics.FromImage(finalImage);
            graphics.CompositingMode = CompositingMode.SourceOver;

            graphics.DrawImage(baseImage, 0, 0);
            graphics.DrawImage(overlayImage, 0, 0);

            picPreview.Image = finalImage;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Image<Gray, float> image = new Image<Gray, float>(@"C:\Users\Богдан\Desktop\SecondLab\cln1.jpg");

            picPreview.Image = image.ToBitmap(picPreview.Width, picPreview.Height);

            //IntPtr complexImage = CvInvoke.cvCreateImage(image.Size, IPL_DEPTH.IPL_DEPTH_32F, 2);


            //CvInvoke.cvSetZero(complexImage);  // Initialize all elements to Zero
            //CvInvoke.cvSetImageCOI(complexImage, 1);
            //CvInvoke.cvCopy(image, complexImage, IntPtr.Zero);
            //CvInvoke.cvSetImageCOI(complexImage, 0);

            //Matrix<float> dft = new Matrix<float>(image.Rows, image.Cols, 2);
            //CvInvoke.cvDFT(complexImage, dft, CV_DXT.CV_DXT_FORWARD, 0);


            //Matrix<float> outReal = new Matrix<float>(image.Size);

            //Matrix<float> outIm = new Matrix<float>(image.Size);
            //CvInvoke.cvSplit(dft, outReal, outIm, IntPtr.Zero, IntPtr.Zero);

            //CvInvoke.cvPow(outReal, outReal, 2.0);
            //CvInvoke.cvPow(outIm, outIm, 2.0);

            //CvInvoke.cvAdd(outReal, outIm, outReal, IntPtr.Zero);
            //CvInvoke.cvPow(outReal, outReal, 0.5);

            //CvInvoke.cvAdd(outReal, outIm, outReal, IntPtr.Zero); // 1 + Mag
            //CvInvoke.cvLog(outReal, outReal); // log(1 + Mag)


            //int cx = outReal.Cols / 2;
            //int cy = outReal.Rows / 2;

            //Matrix<float> q0 = outReal.GetSubRect(new Rectangle(0, 0, cx, cy));
            //Matrix<float> q1 = outReal.GetSubRect(new Rectangle(cx, 0, cx, cy));
            //Matrix<float> q2 = outReal.GetSubRect(new Rectangle(0, cy, cx, cy));
            //Matrix<float> q3 = outReal.GetSubRect(new Rectangle(cx, cy, cx, cy));
            //Matrix<float> tmp = new Matrix<float>(q0.Size);

            //q0.CopyTo(tmp);
            //q3.CopyTo(q0);
            //tmp.CopyTo(q3);
            //q1.CopyTo(tmp);
            //q2.CopyTo(q1);
            //tmp.CopyTo(q2);

            //CvInvoke.cvNormalize(outReal, outReal, 0.0, 255.0, NORM_TYPE.CV_MINMAX, IntPtr.Zero);

            //Image<Gray, float> fftImage = new Image<Gray, float>(outReal.Size);
            //CvInvoke.cvCopy(outReal, fftImage, IntPtr.Zero);

            ////pictureBox1.Image = image.ToBitmap();
            //picPreview.Image = fftImage.ToBitmap();
        }
    }
}
