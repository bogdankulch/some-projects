﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShipCount
{
    class Program
    {
        private static int[,] _battleField = new int[,] {
        { 1,1,0,1,0,0},
        { 1,1,0,1,0,1},
        { 0,0,0,1,0,1},
        { 1,0,0,0,0,0}
        };

        static void Main(string[] args)
        {
            var countOfShip = GetCountOfBattleShips(_battleField);

            Console.WriteLine(countOfShip);

            Console.ReadLine();
        }

        public static int GetCountOfBattleShips(int[,] arr)
        {
            int result = 0;


            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    if (arr[i, j] == 1)
                    {
                        GetOneShip(arr, i, j);
                        result++;
                    }
                }
            }

            return result;
        }

        public static void GetOneShip(int[,] arr, int i, int j)
        {
            Queue<int[]> queue = new Queue<int[]>();

            arr[i, j] = 0;
            queue.Enqueue(new int[] { i, j });

            while (queue.Count > 0)
            {
                int[] a = queue.Dequeue();

                int t_i = a[0], t_j = a[1];

                //right
                if (t_i != arr.GetLength(0) - 1 && arr[t_i + 1, t_j] == 1)
                {
                    arr[t_i + 1, t_j] = 0;
                    queue.Enqueue(new int[] { t_i + 1, t_j });
                }


                //bottom
                if (t_j != arr.GetLength(1) - 1 && arr[t_i, t_j + 1] == 1)
                {
                    arr[t_i, t_j + 1] = 0;
                    queue.Enqueue(new int[] { t_i, t_j + 1 });
                }


                //left
                if (t_i != 0 && arr[t_i - 1, t_j] == 1)
                {

                    arr[t_i - 1, t_j] = 0;
                    queue.Enqueue(new int[] { t_i - 1, t_j });
                }


                //top
                if (t_j != 0 && arr[t_i, t_j - 1] == 1)
                {
                    arr[t_i, t_j - 1] = 0;
                    queue.Enqueue(new int[] { t_i, t_j - 1 });
                }
            }

        }

    }
}
