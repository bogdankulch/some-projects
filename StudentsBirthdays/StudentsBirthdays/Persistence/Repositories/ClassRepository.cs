﻿using StudentsBirthdays.Core.Domain;
using StudentsBirthdays.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsBirthdays.Persistence.Repositories
{ 
    public class ClassRepository : Repository<Class>, IClassRepository
    {
        public StudentsBirthdaysContext db => Context as StudentsBirthdaysContext;
        public ClassRepository(StudentsBirthdaysContext context) : base(context)
        {
        }

        public List<string> GetStudentsBirthdayForClass()
        {
            List<string> studentBirthday = new List<string>();
            studentBirthday = db.Students.Where(b => b.Birthday.Month == DateTime.Now.Month && b.ClassID == 2).OrderBy(b => b.Birthday).Select(b => b.FirstName + b.LastName + b.ClassID + b.Birthday).ToList();
            foreach (var b in studentBirthday) ;
            return studentBirthday;
        }
    }
}
