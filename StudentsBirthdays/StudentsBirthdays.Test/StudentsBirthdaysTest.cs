﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentsBirthdays.Persistence;
using StudentsBirthdays.Services;

namespace StudentsBirthdays.Test
{
    [TestClass]
    public class StudentsBirthdaysTest
    {
        List<string> studentBirthday;
        List<string> studBirthCurr;
        List<string> birthdayMonth;
        List<string> threeOldStudents;
        List<string> threeYoungestStudents;
        List<string> studentBirthdayTest = new List<string> { "Megan Fox" };
        List<string> studBirthCurrTest = new List<string> { "Adi Dasler", "Cherry Terry", "Megan Fox" };
        List<string> birthdayMonthTest = new List<string> { "Kety Perry" };
        List<string> threeOldStudentsTest = new List<string> { "Kety Perry", "Cherry Terry", "Adi Dasler" };
        List<string> threeYoungestStudentsTest = new List<string> { "Megan Fox", "Adi Dasler", "Cherry Terry" };

        StudentsBirthdaysContext studentContext = new StudentsBirthdaysContext();
        StudentsServices studentService;

        [TestMethod]
        public void Students_Service()
        {
            studentService = new StudentsServices(studentContext);
            Assert.IsNotNull(studentService);
        }

        [TestMethod()]
        public void GetStudentsBirthdayForClass()
        {
            studentService = new StudentsServices(studentContext);

            studentBirthday = studentService.GetStudentsBirthdayForClass();

            CollectionAssert.AreEqual(studentBirthday, studentBirthdayTest);
        }

        [TestMethod()]
        public void GetStudentsBirthdayInCurrentMonth()
        {
            studentService = new StudentsServices(studentContext);

            studBirthCurr = studentService.GetStudentsBirthdayInCurrentMonth();

            CollectionAssert.AreEqual(studBirthCurr, studBirthCurrTest);
        }

        [TestMethod()]
        public void GetStudentsBirthdayInMonth()
        {
            studentService = new StudentsServices(studentContext);

            birthdayMonth = studentService.GetStudentsBirthdayInMonth();

            CollectionAssert.AreEqual(birthdayMonth, birthdayMonthTest);
        }

        [TestMethod()]
        public void GetThreeOldestStudents()
        {
            studentService = new StudentsServices(studentContext);

            threeOldStudents = studentService.GetThreeOldestStudents();

            CollectionAssert.AreEqual(threeOldStudents, threeOldStudentsTest);
        }

        [TestMethod()]
        public void GetThreeYoungestStudents()
        {
            studentService = new StudentsServices(studentContext);

            threeYoungestStudents = studentService.GetThreeYoungestStudents();

            CollectionAssert.AreEqual(threeYoungestStudents, threeYoungestStudentsTest);
        }
    }
}
