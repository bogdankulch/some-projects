﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GooglePlacePhotos;
using static GooglePlacePhotos.PlaceClasses;
using System.Collections.Generic;
using System.IO;

namespace GoogleApiTest
{
    [TestClass]
    public class GooglePlaceTest
    {
        public IGooglePlaceService googlePlaceService = new GooglePlace();

        private string name = "Lviv%20Opera%20Theater";
        private int radius = 5000;
        private string language = "en";
        private string key = "AIzaSyATN55LsIv3qlv7v_ny6pJwPsB5rKn5JhI";

        [TestMethod]
        public void ResponceIsSuccess()
        {
            string request = googlePlaceService.CreateReqest(name, radius, language, key);
            googlePlaceService.Request(request);

        }

        [TestMethod]
        public void TestResult()
        {
            string request = googlePlaceService.CreateReqest(name, radius, language, key);
            string json = googlePlaceService.Request(request);
            Result result = googlePlaceService.Responce(json);

            bool correct = true;

            if (result.status != "OK")
                correct = false;


            Assert.IsTrue(correct);
        }
    }
}

