﻿// <auto-generated />
namespace StudentsBirthdays.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.3.0")]
    public sealed partial class CreateStudent : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateStudent));
        
        string IMigrationMetadata.Id
        {
            get { return "201910061733227_CreateStudent"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
