﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Net
{
    class Program3
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 24; i++)
            {
                if (i % 10 < 6)
                {
                    string h = i.ToString("00");
                    Console.WriteLine($"{h}:{h[1]}{h[0]}");
                }
            }
            Console.ReadKey();
        }
    }
}

