﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Net
{
    class Program2
    {
        static void printRepeating(int[] arr, int size)
        {
            int i, j;

            Console.Write("Repeated Elements are: ");
            for (i = 0; i < size; i++)
            {
                for (j = i + 1; j < size; j++)
                {
                    if (arr[i] == arr[j])

                        Console.Write(arr[i] + " ");
                }
            }
        }

        public static void Main()
        {
            int[] arr = Enumerable.Range(1, 100).ToArray();
            arr[11] = arr[88];

            int arr_size = arr.Length;

            printRepeating(arr, arr_size);

            Console.ReadKey();
        }
    }
}
