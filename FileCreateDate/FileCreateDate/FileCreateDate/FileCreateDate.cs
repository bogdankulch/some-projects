﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCreateDate
{
    public class FileCreateDate
    {
        public void File_Create_Date(string TaskFolder, string FileResult)
        {
            var files = Directory.GetFiles(TaskFolder, "*.*", SearchOption.AllDirectories).OrderBy(d => new FileInfo(d).CreationTime);
            using(StreamWriter sw = new StreamWriter(FileResult, false))
                foreach (var file in files)
                {
                    DateTime fileCreatedDate = File.GetCreationTime(file);
                    sw.WriteLine(file + " " + fileCreatedDate);
                }
        }
    }
}