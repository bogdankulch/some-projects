﻿using StudentsBirthdays.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsBirthdays
{
   public class StudentsBirthdaysContext : DbContext
    {
        public StudentsBirthdaysContext() : base("StudentsBirthdaysContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Class> Classes { get; set; }
    }
}
