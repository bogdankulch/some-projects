﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsBirthdays.Core.Domain
{
   public class Class
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Student> Students { get; set; }
        public Class()
        {
            Students = new List<Student>();
        }
    }
}
