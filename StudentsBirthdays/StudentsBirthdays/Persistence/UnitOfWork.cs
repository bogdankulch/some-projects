﻿using StudentsBirthdays.Core;
using StudentsBirthdays.Core.Repositories;
using StudentsBirthdays.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsBirthdays.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly StudentsBirthdaysContext _context;

        public UnitOfWork(StudentsBirthdaysContext context)
        {
            _context = context;
            Classes = new ClassRepository(_context);
            Students = new StudentRepository(_context);
        }
        public IClassRepository Classes { get; private set; }
        public IStudentRepository Students { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
