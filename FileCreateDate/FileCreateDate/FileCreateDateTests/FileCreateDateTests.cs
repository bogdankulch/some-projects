﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileCreateDate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileCreateDate.Tests
{
    [TestClass()]
    public class FileCreateDateTests
    {
        [TestMethod()]
        public void FileCreateDateTest()
        {
            string TaskFolder = @"D:\TaskFolder";
            string FileResult = @"D:\Task.txt";

            int expected_amount_rows_in_file = 7;

            FileCreateDate fileCrete = new FileCreateDate();
            fileCrete.File_Create_Date(TaskFolder, FileResult);

            int actual_files = File.ReadAllLines(FileResult).Length;

            Assert.AreEqual(expected_amount_rows_in_file, actual_files);
        }
    }
}