﻿using StudentsBirthdays.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsBirthdays.Core.Repositories
{
    public interface IClassRepository : IRepository<Class>
    {
        List<string> GetStudentsBirthdayForClass();
    }
}
