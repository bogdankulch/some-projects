﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsBirthdays.Core.Domain
{
   public class Student
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ClassID { get; set; }
        public DateTime Birthday { get; set; }

        public virtual Class Class { get; set; }
    }
}
