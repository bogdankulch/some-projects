﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GooglePlacePhotos.PlaceClasses;

namespace GooglePlacePhotos
{
    public interface IGooglePlaceService
    {
        string CreateReqest(string name, int radius, string language, string key);

        string Request(string request);

        Result Responce(string json);
    }
}
